# ytimg

> **(May 2021)** moved to [md0.org/ytimg](https://md0.org/ytimg).

ytimg downloads the maxresdefault image associated with the provided
youtube video URL.

```
usage: ytimg [-o filename] <video-url>
```

Example:

given:

```
https://www.youtube.com/watch?v=sOiuE8Fn-Ro
```

fetches:

```
https://img.youtube.com/vi/sOiuE8Fn-Ro/maxresdefault.jpg
```
