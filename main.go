// ytimg downloads the maxresdefault image associated with the provided
// youtube video URL.
//
//   given
//     https://www.youtube.com/watch?v=sOiuE8Fn-Ro
//   fetches
//     https://img.youtube.com/vi/sOiuE8Fn-Ro/maxresdefault.jpg
//
// Ways this can go wrong (all handled):
// - no url given on command line
// - url given, isn't youtube url
// - url given, is malformed youtube url
// - -o provided, doesn't end in "jpg"
// - local file already exists
//
// Of course this is a brittle, and it would probably be better to use
// the youtube API, but this was quick and as long as it works, it
// works!
package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"

	"md0.org/fileutil"
)

const VERSION = "1.0.0"

func usage() {
	fmt.Fprintf(os.Stderr, "usage: ytimg [-o filename] <video-url>\n")
	os.Exit(2)
}

func main() {
	var (
		defaultName = "maxresdefault.jpg"
		local       = defaultName
		outfile     = flag.String("o", "", "optional output filename")
		v           = flag.Bool("v", false, "print version")
	)

	log.SetPrefix("ytimg: ")
	log.SetFlags(0)
	flag.Usage = usage
	flag.Parse()
	if *v {
		fmt.Println(VERSION)
		return
	}
	if flag.NArg() != 1 {
		flag.Usage()
	}

	videoURL, err := url.Parse(flag.Arg(0))
	if err != nil {
		log.Print(err)
		flag.Usage()
	}

	// little bit of ad hoc validation here...

	if videoURL.Hostname() != "www.youtube.com" || videoURL.Path != "/watch" {
		flag.Usage()
	}

	key := videoURL.Query().Get("v")
	if key == "" {
		flag.Usage()
	}

	if *outfile != "" {
		local = *outfile
	}

	if !strings.HasSuffix(local, ".jpg") {
		log.Print("local filename doesn't end in '.jpg'")
		os.Exit(3)
	}

	if fileutil.IsExist(local) {
		log.Printf("local file exists: %s", local)
		os.Exit(4)
	}

	imageURL := fmt.Sprintf("https://img.youtube.com/vi/%s/%s", key, defaultName)
	n, err := fetch(imageURL, local)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("fetched %d bytes into %s\n", n, local)
}

// Fetch downloads the URL and returns the length of the local file.
// Inspired/(copied) from GOPL, p. 148.  This version adds the local
// filename as a function argument, main having already taken care of
// issues around the local name.
func fetch(url string, local string) (n int64, err error) {
	resp, err := http.Get(url)
	if err != nil {
		return 0, err
	}
	defer resp.Body.Close()

	f, err := os.Create(local)
	if err != nil {
		return 0, err
	}
	n, err = io.Copy(f, resp.Body)
	// Close file, but prefer error from Copy, if any.
	if closeErr := f.Close(); err == nil {
		err = closeErr
	}
	return n, err
}
